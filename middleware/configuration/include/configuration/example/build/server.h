//! 
//! Copyright (c) 2015, The casual project
//!
//! This software is licensed under the MIT license, https://opensource.org/licenses/MIT
//!


#pragma once

#include "configuration/build/server.h"

namespace casual
{
   namespace configuration
   {
      namespace example
      {
         namespace build
         {
            namespace server
            {
               configuration::build::server::Server example();

            } // server

         } // build

      } // example
   } // configuration
} // casual


