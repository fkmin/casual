# configuration domain

This is the runtime configuration for a casual domain.

Most of the _sections_ has a property `note` for the user to provide descriptions of the documentation. 
The examples further down uses this to make it easier to understand the examples by them self. But can of
course be used as a mean to help document actual production configuration.  

The following _sections_ can be used:

## transaction

Defines transaction related configuration.

### log

The path of the distributed transactions log file. When a distributed transaction reaches prepare,
this state is persistent stored, before the actual commit stage.

if `:memory:` is used, the log is non-persistent. 

### resources

Defines all resources that `servers` and `executables` can be associated with, within this configuration.

property       | description
---------------|----------------------------------------------------
key            | the resource key - correlates to a defined resource in the resource.property (for current _machine_)
name           | a user defined name, used to correlate this resource within the rest of the configuration. 
instances      | number of resource-proxy instances transaction-manger should start. To handle distributed transactions
openinfo       | resources specific _open_ configurations for the particular resource.
closeinfo      | resources specific _close_ configurations for the particular resource.


## groups

Defines the groups in the configuration. Groups are used to associate `resources` to serveres/executables
and to define the dependency order.

property       | description
---------------|----------------------------------------------------
name           | the name (unique key) of the group.
dependencies   | defines which other groups this group has dependency to.
resources      | defines which resources (name) this group associate with (transient to the members of the group)


## servers

Defines all servers of the configuration (and domain) 

property       | description
---------------|----------------------------------------------------
path           | the path to the binary, can be relative to `CASUAL_DOMAIN_HOME`, or implicit via `PATH`.
alias          | the logical (unique) name of the server. If not provided basename of `path` will be used
arguments      | arguments to `tpsvrinit` during startup.
instances      | number of instances to start of the server.
memberships    | which groups are the server member of (dictates order, and possible resource association)
restrictions   | service restrictions, if provided the intersection of _restrictions_ and advertised services are actually advertised.
resources      | explicit resource associations (transaction.resources.name)


## executables

Defines all _ordinary_ executables of the configuration. Could be any executable with a `main` function

property       | description
---------------|----------------------------------------------------
path           | the path to the binary, can be relative to `CASUAL_DOMAIN_HOME`, or implicit via `PATH`.
alias          | the logical (unique) name of the executable. If not provided basename of `path` will be used
arguments      | arguments to `main` during startup.
instances      | number of instances to start of the server.
memberships    | which groups are the server member of (dictates order)


## services

Used for configuring specific attribute on named services.

Note that this configuration is tied to the service, regardless who has advertised the service.

property       | description
---------------|----------------------------------------------------
name           | name of the service
timeout (deprecated)        | timeout of the service, from the _caller_ perspective (example: `30ms`, `1h`, `3min`, `40s`. if no SI unit `s` is used)
execution.timeout.duration | timeout of the service, from the _caller_ perspective (example: `30ms`, `1h`, `3min`, `40s`. if no SI unit `s` is used)
execution.timeout.contract       | defines action to take if timeout is passed (linger = just wait, kill = send kill signal, terminate = send terminate signal)
routes         | defines what logical names are actually exposed. For _aliases_, it's important to include the original name.


## gateway

Defines configuration for communication with other `casual` domains.

### inbound

Defines all inbound related configuration (from remote domains -> local domain)

### default
#### inbound

property       | description
---------------|----------------------------------------------------
limit.size     | default value for limit size 
limit.messages | default value for maximum number of messages

#### groups

Defines a list of all inbound groups

property       | description
---------------|----------------------------------------------------
alias          | an _identity_ for this group instance (if not set, casual generates one)
connections    | all the connections for this group

##### connection

property       | description
---------------|----------------------------------------------------
address        | the address to listen on, `host:port` 
limit.size     | the maximum allowed size of all inflight messages. If reached _inbound_ stop taking more request until below the limit 
limit.messages | the maximum allowed number of inflight messages. If reached _inbound_ stop taking more request until below the limit

### outbound

Defines all outbound related configuration (from local domain -> remote domains)

#### groups

Defines a list of all outbound groups. 

Each group gets an _order_ in the order they are defined. Groups defined lower down will only be used if the higher
ups does not provide the wanted _service_ or _queue_. Hence, the lower downs can be used as _fallback_.

property       | description
---------------|----------------------------------------------------
alias          | an _identity_ for this group instance (if not set, casual generates one)
connections    | all the connections for this group

##### connection

Defines all connections that this _outbound group_ should try to connect to.

All connections within a group ar treated equal, and service calls will be load balanced with _round robin_. Allthough,
`casual` will try to _route_ the same transaction to the previous _associated_ connectino with the specific transaction. 
This is only done to minimize the amount of _resources_ involved within the prepare and commit/rollback stage.  


property       | description
---------------|----------------------------------------------------
address        | the address to connect to, `host:port` 
services       | services we're expecting to find on the other side 
queues         | queues we're expecting to find on the other side 

`services` and `queues` is used as an _optimization_ to do a _build_ discovery during startup. `casual`
will find these services later lazily otherwise. It can also be used to do some rudimentary load balancing 
to make sure lower prioritized connections are used for `services` and `queues` that could be discovered in
higher prioritized connections.

## queue

Defines the queue configuration

### groups

Defines groups of queues which share the same storage location. Groups has no other meaning.

property       | description
---------------|----------------------------------------------------
name           | the (unique) name of the group.
queuebase      | the path to the storage file. (default: `${CASUAL_DOMAIN_HOM}/queue/<group-name>.qb`, if `:memory:` is used, the storage is non persistent)
queues         | defines all queues in this group, see below

#### groups.queues

property       | description
---------------|----------------------------------------------------
name           | the (unique) name of the queue.
retry.count    | number of rollbacks before moving message to `<queue-name>.error`.
retry.delay    | if message is rolled backed, how long delay before message is avaliable for dequeue. (example: `30ms`, `1h`, `3min`, `40s`. if no SI unit `s` is used)

## service
Defines _global_ service information that will be used as configuration on services not specifying specific values in the _services_ section.

property                         | description
---------------------------------|----------------------------------------------------
execution.timeout.duration       | timeout of service, from the _caller_ perspective (example: `30ms`, `1h`, `3min`, `40s`. if no SI unit `s` is used)
execution.timeout.contract       | defines action to take if timeout is passed (linger = just wait, kill = send kill signal, terminate = send terminate signal)

## examples 

Below follows examples in human readable formats that `casual` can handle

### yaml
```` yaml
---
domain:
  name: "domain.A42"
  default:
    server:
      instances: 1
      restart: true
    executable:
      instances: 1
      restart: false
    service:
      timeout: "90s"
  environment:
    variables:
      - key: "SOME_VARIABLE"
        value: "42"
      - key: "SOME_OTHER_VARIABLE"
        value: "some value"
  transaction:
    default:
      resource:
        key: "db2_rm"
        instances: 3
    log: "/some/fast/disk/domain.A42/transaction.log"
    resources:
      - name: "customer-db"
        key: "db2_rm"
        instances: 5
        note: "this resource is named 'customer-db' - using the default rm-key (db_rm) - overrides the default rm-instances to 5"
        openinfo: "db=customer,uid=db2,pwd=db2"
      - name: "sales-db"
        key: "db2_rm"
        instances: 3
        note: "this resource is named 'sales-db' - using the default rm-key (db_rm) - using default rm-instances"
        openinfo: "db=sales,uid=db2,pwd=db2"
      - name: "event-queue"
        key: "mq_rm"
        instances: 3
        note: "this resource is named 'event-queue' - overrides rm-key - using default rm-instances"
        openinfo: "some-mq-specific-stuff"
        closeinfo: "some-mq-specific-stuff"
  groups:
    - name: "common-group"
      note: "group that logically groups 'common' stuff"
    - name: "customer-group"
      note: "group that logically groups 'customer' stuff"
      resources:
        - "customer-db"
      dependencies:
        - "common-group"
    - name: "sales-group"
      note: "group that logically groups 'customer' stuff"
      resources:
        - "sales-db"
        - "event-queue"
      dependencies:
        - "customer-group"
  servers:
    - path: "customer-server-1"
      instances: 1
      memberships:
        - "customer-group"
      restart: true
    - path: "customer-server-2"
      instances: 1
      memberships:
        - "customer-group"
      restart: true
    - path: "sales-server"
      alias: "sales-pre"
      note: "the only services that will be advertised are 'preSalesSaveService' and 'preSalesGetService'"
      instances: 10
      memberships:
        - "sales-group"
      restart: true
      restrictions:
        - "preSalesSaveService"
        - "preSalesGetService"
    - path: "sales-server"
      alias: "sales-post"
      note: "the only services that will be advertised are 'postSalesSaveService' and 'postSalesGetService'"
      instances: 1
      memberships:
        - "sales-group"
      restart: true
      restrictions:
        - "postSalesSaveService"
        - "postSalesGetService"
    - path: "sales-broker"
      instances: 1
      memberships:
        - "sales-group"
      environment:
        variables:
          - key: "SALES_BROKER_VARIABLE"
            value: "556"
      restart: true
      resources:
        - "event-queue"
  executables:
    - path: "mq-server"
      arguments:
        - "--configuration"
        - "/path/to/configuration"
      instances: 1
      memberships:
        - "common-group"
      restart: false
  services:
    - name: "postSalesSaveService"
      timeout: "2h"
      routes:
        - "postSalesSaveService"
        - "sales/post/save"
    - name: "postSalesGetService"
      timeout: "130ms"
  gateway:
    inbound:
      groups:
        - alias: "unique-inbound-alias"
          note: "if threshold of 2MB of total payload 'in flight' is reach inbound will stop consume from socket until we're below"
          limit:
            size: 2097152
          connections:
            - address: "localhost:7779"
              note: "can be several listening host:port per inbound instance"
            - address: "some.host.org:7779"
        - note: "(generated alias) listeners - threshold of either 10 messages OR 10MB - the first that is reach, inbound will stop consume"
          limit:
            size: 10485760
            messages: 10
          connections:
            - address: "some.host.org:7780"
            - address: "some.host.org:4242"
        - note: "(generated alias) listeners - no limits"
          connections:
            - address: "some.host.org:4242"
    outbound:
      groups:
        - alias: "primary"
          note: "casual will 'round-robin' between connections within a group for the same service/queue"
          connections:
            - address: "a45.domain.host.org:7779"
              note: "connection to domain 'a45' - we expect to find service 's1' and 's2' there."
              services:
                - "s1"
                - "s2"
            - address: "a46.domain.host.org:7779"
              note: "we expect to find queues 'q1' and 'q2' and service 's1'"
              services:
                - "s1"
              queues:
                - "q1"
                - "q2"
        - alias: "fallback"
          connections:
            - address: "a99.domain.host.org:7780"
              note: "will be chosen if _resources_ are not found at connections in the 'primary' outbound"
    reverse:
      inbound:
        groups:
          - alias: "unique-alias-name"
            note: "connect to other reverse outbound that is listening on this port - then treat it as a regular inbound"
            limit:
              messages: 42
            connections:
              - address: "localhost:7780"
                note: "one of possible many addresses to connect to"
      outbound:
        groups:
          - alias: "primary"
            note: "listen for connection from reverse inbound - then treat it as a regular outbound"
            connections:
              - address: "localhost:7780"
                note: "one of possible many listining addresses."
          - alias: "secondary"
            note: "onther instance (proces) that handles (multiplexed) traffic on it's own"
            connections:
              - address: "localhost:7781"
                note: "one of possible many listining addresses."
  queue:
    note: "retry.count - if number of rollbacks is greater, message is moved to error-queue  retry.delay - the amount of time before the message is available for consumption, after rollback\n"
    default:
      queue:
        retry:
          count: 3
          delay: "20s"
      directory: "${CASUAL_DOMAIN_HOME}/queue/groups"
    groups:
      - alias: "A"
        note: "will get default queuebase: ${CASUAL_DOMAIN_HOME}/queue/groupA.gb"
        queues:
          - name: "a1"
          - name: "a2"
            retry:
              count: 10
              delay: "100ms"
            note: "after 10 rollbacked dequeues, message is moved to a2.error"
          - name: "a3"
          - name: "a4"
      - alias: "B"
        queuebase: "/some/fast/disk/queue/groupB.qb"
        queues:
          - name: "b1"
          - name: "b2"
            retry:
              count: 20
            note: "after 20 rollbacked dequeues, message is moved to b2.error. retry.delay is 'inherited' from default, if any"
      - queuebase: ":memory:"
        note: "group is an in-memory queue, hence no persistence"
        queues:
          - name: "c1"
          - name: "c2"
        name: "C"
    forward:
      default:
        service:
          instances: 3
          reply:
            delay: "2s"
        queue:
          instances: 1
          target:
            delay: "500ms"
      groups:
        - alias: "forward-group-1"
          services:
            - source: "b1"
              instances: 4
              target:
                service: "casual/example/echo"
              reply:
                queue: "a3"
                delay: "10ms"
          queues:
            - source: "c1"
              target:
                queue: "a4"
        - alias: "forward-group-2"
          services:
            - source: "b2"
              target:
                service: "casual/example/echo"
...

````
### json
```` json
{
    "domain": {
        "name": "domain.A42",
        "default": {
            "server": {
                "instances": 1,
                "restart": true
            },
            "executable": {
                "instances": 1,
                "restart": false
            },
            "service": {
                "timeout": "90s"
            }
        },
        "environment": {
            "variables": [
                {
                    "key": "SOME_VARIABLE",
                    "value": "42"
                },
                {
                    "key": "SOME_OTHER_VARIABLE",
                    "value": "some value"
                }
            ]
        },
        "transaction": {
            "default": {
                "resource": {
                    "key": "db2_rm",
                    "instances": 3
                }
            },
            "log": "/some/fast/disk/domain.A42/transaction.log",
            "resources": [
                {
                    "name": "customer-db",
                    "key": "db2_rm",
                    "instances": 5,
                    "note": "this resource is named 'customer-db' - using the default rm-key (db_rm) - overrides the default rm-instances to 5",
                    "openinfo": "db=customer,uid=db2,pwd=db2"
                },
                {
                    "name": "sales-db",
                    "key": "db2_rm",
                    "instances": 3,
                    "note": "this resource is named 'sales-db' - using the default rm-key (db_rm) - using default rm-instances",
                    "openinfo": "db=sales,uid=db2,pwd=db2"
                },
                {
                    "name": "event-queue",
                    "key": "mq_rm",
                    "instances": 3,
                    "note": "this resource is named 'event-queue' - overrides rm-key - using default rm-instances",
                    "openinfo": "some-mq-specific-stuff",
                    "closeinfo": "some-mq-specific-stuff"
                }
            ]
        },
        "groups": [
            {
                "name": "common-group",
                "note": "group that logically groups 'common' stuff"
            },
            {
                "name": "customer-group",
                "note": "group that logically groups 'customer' stuff",
                "resources": [
                    "customer-db"
                ],
                "dependencies": [
                    "common-group"
                ]
            },
            {
                "name": "sales-group",
                "note": "group that logically groups 'customer' stuff",
                "resources": [
                    "sales-db",
                    "event-queue"
                ],
                "dependencies": [
                    "customer-group"
                ]
            }
        ],
        "servers": [
            {
                "path": "customer-server-1",
                "instances": 1,
                "memberships": [
                    "customer-group"
                ],
                "restart": true
            },
            {
                "path": "customer-server-2",
                "instances": 1,
                "memberships": [
                    "customer-group"
                ],
                "restart": true
            },
            {
                "path": "sales-server",
                "alias": "sales-pre",
                "note": "the only services that will be advertised are 'preSalesSaveService' and 'preSalesGetService'",
                "instances": 10,
                "memberships": [
                    "sales-group"
                ],
                "restart": true,
                "restrictions": [
                    "preSalesSaveService",
                    "preSalesGetService"
                ]
            },
            {
                "path": "sales-server",
                "alias": "sales-post",
                "note": "the only services that will be advertised are 'postSalesSaveService' and 'postSalesGetService'",
                "instances": 1,
                "memberships": [
                    "sales-group"
                ],
                "restart": true,
                "restrictions": [
                    "postSalesSaveService",
                    "postSalesGetService"
                ]
            },
            {
                "path": "sales-broker",
                "instances": 1,
                "memberships": [
                    "sales-group"
                ],
                "environment": {
                    "variables": [
                        {
                            "key": "SALES_BROKER_VARIABLE",
                            "value": "556"
                        }
                    ]
                },
                "restart": true,
                "resources": [
                    "event-queue"
                ]
            }
        ],
        "executables": [
            {
                "path": "mq-server",
                "arguments": [
                    "--configuration",
                    "/path/to/configuration"
                ],
                "instances": 1,
                "memberships": [
                    "common-group"
                ],
                "restart": false
            }
        ],
        "services": [
            {
                "name": "postSalesSaveService",
                "timeout": "2h",
                "routes": [
                    "postSalesSaveService",
                    "sales/post/save"
                ]
            },
            {
                "name": "postSalesGetService",
                "timeout": "130ms"
            }
        ],
        "gateway": {
            "inbound": {
                "groups": [
                    {
                        "alias": "unique-inbound-alias",
                        "note": "if threshold of 2MB of total payload 'in flight' is reach inbound will stop consume from socket until we're below",
                        "limit": {
                            "size": 2097152
                        },
                        "connections": [
                            {
                                "address": "localhost:7779",
                                "note": "can be several listening host:port per inbound instance"
                            },
                            {
                                "address": "some.host.org:7779"
                            }
                        ]
                    },
                    {
                        "note": "(generated alias) listeners - threshold of either 10 messages OR 10MB - the first that is reach, inbound will stop consume",
                        "limit": {
                            "size": 10485760,
                            "messages": 10
                        },
                        "connections": [
                            {
                                "address": "some.host.org:7780"
                            },
                            {
                                "address": "some.host.org:4242"
                            }
                        ]
                    },
                    {
                        "note": "(generated alias) listeners - no limits",
                        "connections": [
                            {
                                "address": "some.host.org:4242"
                            }
                        ]
                    }
                ]
            },
            "outbound": {
                "groups": [
                    {
                        "alias": "primary",
                        "note": "casual will 'round-robin' between connections within a group for the same service/queue",
                        "connections": [
                            {
                                "address": "a45.domain.host.org:7779",
                                "note": "connection to domain 'a45' - we expect to find service 's1' and 's2' there.",
                                "services": [
                                    "s1",
                                    "s2"
                                ]
                            },
                            {
                                "address": "a46.domain.host.org:7779",
                                "note": "we expect to find queues 'q1' and 'q2' and service 's1'",
                                "services": [
                                    "s1"
                                ],
                                "queues": [
                                    "q1",
                                    "q2"
                                ]
                            }
                        ]
                    },
                    {
                        "alias": "fallback",
                        "connections": [
                            {
                                "address": "a99.domain.host.org:7780",
                                "note": "will be chosen if _resources_ are not found at connections in the 'primary' outbound"
                            }
                        ]
                    }
                ]
            },
            "reverse": {
                "inbound": {
                    "groups": [
                        {
                            "alias": "unique-alias-name",
                            "note": "connect to other reverse outbound that is listening on this port - then treat it as a regular inbound",
                            "limit": {
                                "messages": 42
                            },
                            "connections": [
                                {
                                    "address": "localhost:7780",
                                    "note": "one of possible many addresses to connect to"
                                }
                            ]
                        }
                    ]
                },
                "outbound": {
                    "groups": [
                        {
                            "alias": "primary",
                            "note": "listen for connection from reverse inbound - then treat it as a regular outbound",
                            "connections": [
                                {
                                    "address": "localhost:7780",
                                    "note": "one of possible many listining addresses."
                                }
                            ]
                        },
                        {
                            "alias": "secondary",
                            "note": "onther instance (proces) that handles (multiplexed) traffic on it's own",
                            "connections": [
                                {
                                    "address": "localhost:7781",
                                    "note": "one of possible many listining addresses."
                                }
                            ]
                        }
                    ]
                }
            }
        },
        "queue": {
            "note": "retry.count - if number of rollbacks is greater, message is moved to error-queue  retry.delay - the amount of time before the message is available for consumption, after rollback\n",
            "default": {
                "queue": {
                    "retry": {
                        "count": 3,
                        "delay": "20s"
                    }
                },
                "directory": "${CASUAL_DOMAIN_HOME}/queue/groups"
            },
            "groups": [
                {
                    "alias": "A",
                    "note": "will get default queuebase: ${CASUAL_DOMAIN_HOME}/queue/groupA.gb",
                    "queues": [
                        {
                            "name": "a1"
                        },
                        {
                            "name": "a2",
                            "retry": {
                                "count": 10,
                                "delay": "100ms"
                            },
                            "note": "after 10 rollbacked dequeues, message is moved to a2.error"
                        },
                        {
                            "name": "a3"
                        },
                        {
                            "name": "a4"
                        }
                    ]
                },
                {
                    "alias": "B",
                    "queuebase": "/some/fast/disk/queue/groupB.qb",
                    "queues": [
                        {
                            "name": "b1"
                        },
                        {
                            "name": "b2",
                            "retry": {
                                "count": 20
                            },
                            "note": "after 20 rollbacked dequeues, message is moved to b2.error. retry.delay is 'inherited' from default, if any"
                        }
                    ]
                },
                {
                    "queuebase": ":memory:",
                    "note": "group is an in-memory queue, hence no persistence",
                    "queues": [
                        {
                            "name": "c1"
                        },
                        {
                            "name": "c2"
                        }
                    ],
                    "name": "C"
                }
            ],
            "forward": {
                "default": {
                    "service": {
                        "instances": 3,
                        "reply": {
                            "delay": "2s"
                        }
                    },
                    "queue": {
                        "instances": 1,
                        "target": {
                            "delay": "500ms"
                        }
                    }
                },
                "groups": [
                    {
                        "alias": "forward-group-1",
                        "services": [
                            {
                                "source": "b1",
                                "instances": 4,
                                "target": {
                                    "service": "casual/example/echo"
                                },
                                "reply": {
                                    "queue": "a3",
                                    "delay": "10ms"
                                }
                            }
                        ],
                        "queues": [
                            {
                                "source": "c1",
                                "target": {
                                    "queue": "a4"
                                }
                            }
                        ]
                    },
                    {
                        "alias": "forward-group-2",
                        "services": [
                            {
                                "source": "b2",
                                "target": {
                                    "service": "casual/example/echo"
                                }
                            }
                        ]
                    }
                ]
            }
        }
    }
}
````
### ini
```` ini

[domain]
name=domain.A42

[domain.default]

[domain.default.executable]
instances=1
restart=false

[domain.default.server]
instances=1
restart=true

[domain.default.service]
timeout=90s

[domain.environment]

[domain.environment.variables]
key=SOME_VARIABLE
value=42

[domain.environment.variables]
key=SOME_OTHER_VARIABLE
value=some value

[domain.executables]
arguments=--configuration
arguments=/path/to/configuration
instances=1
memberships=common-group
path=mq-server
restart=false

[domain.gateway]

[domain.gateway.inbound]

[domain.gateway.inbound.groups]
alias=unique-inbound-alias
note=if threshold of 2MB of total payload 'in flight' is reach inbound will stop consume from socket until we're below

[domain.gateway.inbound.groups.connections]
address=localhost:7779
note=can be several listening host:port per inbound instance

[domain.gateway.inbound.groups.connections]
address=some.host.org:7779

[domain.gateway.inbound.groups.limit]
size=2097152

[domain.gateway.inbound.groups]
note=(generated alias) listeners - threshold of either 10 messages OR 10MB - the first that is reach, inbound will stop consume

[domain.gateway.inbound.groups.connections]
address=some.host.org:7780

[domain.gateway.inbound.groups.connections]
address=some.host.org:4242

[domain.gateway.inbound.groups.limit]
messages=10
size=10485760

[domain.gateway.inbound.groups]
note=(generated alias) listeners - no limits

[domain.gateway.inbound.groups.connections]
address=some.host.org:4242

[domain.gateway.outbound]

[domain.gateway.outbound.groups]
alias=primary
note=casual will 'round-robin' between connections within a group for the same service/queue

[domain.gateway.outbound.groups.connections]
address=a45.domain.host.org:7779
note=connection to domain 'a45' - we expect to find service 's1' and 's2' there.
services=s1
services=s2

[domain.gateway.outbound.groups.connections]
address=a46.domain.host.org:7779
note=we expect to find queues 'q1' and 'q2' and service 's1'
queues=q1
queues=q2
services=s1

[domain.gateway.outbound.groups]
alias=fallback

[domain.gateway.outbound.groups.connections]
address=a99.domain.host.org:7780
note=will be chosen if _resources_ are not found at connections in the 'primary' outbound

[domain.gateway.reverse]

[domain.gateway.reverse.inbound]

[domain.gateway.reverse.inbound.groups]
alias=unique-alias-name
note=connect to other reverse outbound that is listening on this port - then treat it as a regular inbound

[domain.gateway.reverse.inbound.groups.connections]
address=localhost:7780
note=one of possible many addresses to connect to

[domain.gateway.reverse.inbound.groups.limit]
messages=42

[domain.gateway.reverse.outbound]

[domain.gateway.reverse.outbound.groups]
alias=primary
note=listen for connection from reverse inbound - then treat it as a regular outbound

[domain.gateway.reverse.outbound.groups.connections]
address=localhost:7780
note=one of possible many listining addresses.

[domain.gateway.reverse.outbound.groups]
alias=secondary
note=onther instance (proces) that handles (multiplexed) traffic on it's own

[domain.gateway.reverse.outbound.groups.connections]
address=localhost:7781
note=one of possible many listining addresses.

[domain.groups]
name=common-group
note=group that logically groups 'common' stuff

[domain.groups]
dependencies=common-group
name=customer-group
note=group that logically groups 'customer' stuff
resources=customer-db

[domain.groups]
dependencies=customer-group
name=sales-group
note=group that logically groups 'customer' stuff
resources=sales-db
resources=event-queue

[domain.queue]
note=retry.count - if number of rollbacks is greater, message is moved to error-queue  retry.delay - the amount of time before the message is available for consumption, after rollback\n

[domain.queue.default]
directory=${CASUAL_DOMAIN_HOME}/queue/groups

[domain.queue.default.queue]

[domain.queue.default.queue.retry]
count=3
delay=20s

[domain.queue.forward]

[domain.queue.forward.default]

[domain.queue.forward.default.queue]
instances=1

[domain.queue.forward.default.queue.target]
delay=500ms

[domain.queue.forward.default.service]
instances=3

[domain.queue.forward.default.service.reply]
delay=2s

[domain.queue.forward.groups]
alias=forward-group-1

[domain.queue.forward.groups.queues]
source=c1

[domain.queue.forward.groups.queues.target]
queue=a4

[domain.queue.forward.groups.services]
instances=4
source=b1

[domain.queue.forward.groups.services.reply]
delay=10ms
queue=a3

[domain.queue.forward.groups.services.target]
service=casual/example/echo

[domain.queue.forward.groups]
alias=forward-group-2

[domain.queue.forward.groups.services]
source=b2

[domain.queue.forward.groups.services.target]
service=casual/example/echo

[domain.queue.groups]
alias=A
note=will get default queuebase: ${CASUAL_DOMAIN_HOME}/queue/groupA.gb

[domain.queue.groups.queues]
name=a1

[domain.queue.groups.queues]
name=a2
note=after 10 rollbacked dequeues, message is moved to a2.error

[domain.queue.groups.queues.retry]
count=10
delay=100ms

[domain.queue.groups.queues]
name=a3

[domain.queue.groups.queues]
name=a4

[domain.queue.groups]
alias=B
queuebase=/some/fast/disk/queue/groupB.qb

[domain.queue.groups.queues]
name=b1

[domain.queue.groups.queues]
name=b2
note=after 20 rollbacked dequeues, message is moved to b2.error. retry.delay is 'inherited' from default, if any

[domain.queue.groups.queues.retry]
count=20

[domain.queue.groups]
name=C
note=group is an in-memory queue, hence no persistence
queuebase=:memory:

[domain.queue.groups.queues]
name=c1

[domain.queue.groups.queues]
name=c2

[domain.servers]
instances=1
memberships=customer-group
path=customer-server-1
restart=true

[domain.servers]
instances=1
memberships=customer-group
path=customer-server-2
restart=true

[domain.servers]
alias=sales-pre
instances=10
memberships=sales-group
note=the only services that will be advertised are 'preSalesSaveService' and 'preSalesGetService'
path=sales-server
restart=true
restrictions=preSalesSaveService
restrictions=preSalesGetService

[domain.servers]
alias=sales-post
instances=1
memberships=sales-group
note=the only services that will be advertised are 'postSalesSaveService' and 'postSalesGetService'
path=sales-server
restart=true
restrictions=postSalesSaveService
restrictions=postSalesGetService

[domain.servers]
instances=1
memberships=sales-group
path=sales-broker
resources=event-queue
restart=true

[domain.servers.environment]

[domain.servers.environment.variables]
key=SALES_BROKER_VARIABLE
value=556

[domain.services]
name=postSalesSaveService
routes=postSalesSaveService
routes=sales/post/save
timeout=2h

[domain.services]
name=postSalesGetService
timeout=130ms

[domain.transaction]
log=/some/fast/disk/domain.A42/transaction.log

[domain.transaction.default]

[domain.transaction.default.resource]
instances=3
key=db2_rm

[domain.transaction.resources]
instances=5
key=db2_rm
name=customer-db
note=this resource is named 'customer-db' - using the default rm-key (db_rm) - overrides the default rm-instances to 5
openinfo=db=customer,uid=db2,pwd=db2

[domain.transaction.resources]
instances=3
key=db2_rm
name=sales-db
note=this resource is named 'sales-db' - using the default rm-key (db_rm) - using default rm-instances
openinfo=db=sales,uid=db2,pwd=db2

[domain.transaction.resources]
closeinfo=some-mq-specific-stuff
instances=3
key=mq_rm
name=event-queue
note=this resource is named 'event-queue' - overrides rm-key - using default rm-instances
openinfo=some-mq-specific-stuff

````
### xml
```` xml
<?xml version="1.0"?>
<domain>
 <name>domain.A42</name>
 <default>
  <server>
   <instances>1</instances>
   <restart>true</restart>
  </server>
  <executable>
   <instances>1</instances>
   <restart>false</restart>
  </executable>
  <service>
   <timeout>90s</timeout>
  </service>
 </default>
 <environment>
  <variables>
   <element>
    <key>SOME_VARIABLE</key>
    <value>42</value>
   </element>
   <element>
    <key>SOME_OTHER_VARIABLE</key>
    <value>some value</value>
   </element>
  </variables>
 </environment>
 <transaction>
  <default>
   <resource>
    <key>db2_rm</key>
    <instances>3</instances>
   </resource>
  </default>
  <log>/some/fast/disk/domain.A42/transaction.log</log>
  <resources>
   <element>
    <name>customer-db</name>
    <key>db2_rm</key>
    <instances>5</instances>
    <note>this resource is named 'customer-db' - using the default rm-key (db_rm) - overrides the default rm-instances to 5</note>
    <openinfo>db=customer,uid=db2,pwd=db2</openinfo>
   </element>
   <element>
    <name>sales-db</name>
    <key>db2_rm</key>
    <instances>3</instances>
    <note>this resource is named 'sales-db' - using the default rm-key (db_rm) - using default rm-instances</note>
    <openinfo>db=sales,uid=db2,pwd=db2</openinfo>
   </element>
   <element>
    <name>event-queue</name>
    <key>mq_rm</key>
    <instances>3</instances>
    <note>this resource is named 'event-queue' - overrides rm-key - using default rm-instances</note>
    <openinfo>some-mq-specific-stuff</openinfo>
    <closeinfo>some-mq-specific-stuff</closeinfo>
   </element>
  </resources>
 </transaction>
 <groups>
  <element>
   <name>common-group</name>
   <note>group that logically groups 'common' stuff</note>
  </element>
  <element>
   <name>customer-group</name>
   <note>group that logically groups 'customer' stuff</note>
   <resources>
    <element>customer-db</element>
   </resources>
   <dependencies>
    <element>common-group</element>
   </dependencies>
  </element>
  <element>
   <name>sales-group</name>
   <note>group that logically groups 'customer' stuff</note>
   <resources>
    <element>sales-db</element>
    <element>event-queue</element>
   </resources>
   <dependencies>
    <element>customer-group</element>
   </dependencies>
  </element>
 </groups>
 <servers>
  <element>
   <path>customer-server-1</path>
   <instances>1</instances>
   <memberships>
    <element>customer-group</element>
   </memberships>
   <restart>true</restart>
  </element>
  <element>
   <path>customer-server-2</path>
   <instances>1</instances>
   <memberships>
    <element>customer-group</element>
   </memberships>
   <restart>true</restart>
  </element>
  <element>
   <path>sales-server</path>
   <alias>sales-pre</alias>
   <note>the only services that will be advertised are 'preSalesSaveService' and 'preSalesGetService'</note>
   <instances>10</instances>
   <memberships>
    <element>sales-group</element>
   </memberships>
   <restart>true</restart>
   <restrictions>
    <element>preSalesSaveService</element>
    <element>preSalesGetService</element>
   </restrictions>
  </element>
  <element>
   <path>sales-server</path>
   <alias>sales-post</alias>
   <note>the only services that will be advertised are 'postSalesSaveService' and 'postSalesGetService'</note>
   <instances>1</instances>
   <memberships>
    <element>sales-group</element>
   </memberships>
   <restart>true</restart>
   <restrictions>
    <element>postSalesSaveService</element>
    <element>postSalesGetService</element>
   </restrictions>
  </element>
  <element>
   <path>sales-broker</path>
   <instances>1</instances>
   <memberships>
    <element>sales-group</element>
   </memberships>
   <environment>
    <variables>
     <element>
      <key>SALES_BROKER_VARIABLE</key>
      <value>556</value>
     </element>
    </variables>
   </environment>
   <restart>true</restart>
   <resources>
    <element>event-queue</element>
   </resources>
  </element>
 </servers>
 <executables>
  <element>
   <path>mq-server</path>
   <arguments>
    <element>--configuration</element>
    <element>/path/to/configuration</element>
   </arguments>
   <instances>1</instances>
   <memberships>
    <element>common-group</element>
   </memberships>
   <restart>false</restart>
  </element>
 </executables>
 <services>
  <element>
   <name>postSalesSaveService</name>
   <timeout>2h</timeout>
   <routes>
    <element>postSalesSaveService</element>
    <element>sales/post/save</element>
   </routes>
  </element>
  <element>
   <name>postSalesGetService</name>
   <timeout>130ms</timeout>
  </element>
 </services>
 <gateway>
  <inbound>
   <groups>
    <element>
     <alias>unique-inbound-alias</alias>
     <note>if threshold of 2MB of total payload 'in flight' is reach inbound will stop consume from socket until we're below</note>
     <limit>
      <size>2097152</size>
     </limit>
     <connections>
      <element>
       <address>localhost:7779</address>
       <note>can be several listening host:port per inbound instance</note>
      </element>
      <element>
       <address>some.host.org:7779</address>
      </element>
     </connections>
    </element>
    <element>
     <note>(generated alias) listeners - threshold of either 10 messages OR 10MB - the first that is reach, inbound will stop consume</note>
     <limit>
      <size>10485760</size>
      <messages>10</messages>
     </limit>
     <connections>
      <element>
       <address>some.host.org:7780</address>
      </element>
      <element>
       <address>some.host.org:4242</address>
      </element>
     </connections>
    </element>
    <element>
     <note>(generated alias) listeners - no limits</note>
     <connections>
      <element>
       <address>some.host.org:4242</address>
      </element>
     </connections>
    </element>
   </groups>
  </inbound>
  <outbound>
   <groups>
    <element>
     <alias>primary</alias>
     <note>casual will 'round-robin' between connections within a group for the same service/queue</note>
     <connections>
      <element>
       <address>a45.domain.host.org:7779</address>
       <note>connection to domain 'a45' - we expect to find service 's1' and 's2' there.</note>
       <services>
        <element>s1</element>
        <element>s2</element>
       </services>
      </element>
      <element>
       <address>a46.domain.host.org:7779</address>
       <note>we expect to find queues 'q1' and 'q2' and service 's1'</note>
       <services>
        <element>s1</element>
       </services>
       <queues>
        <element>q1</element>
        <element>q2</element>
       </queues>
      </element>
     </connections>
    </element>
    <element>
     <alias>fallback</alias>
     <connections>
      <element>
       <address>a99.domain.host.org:7780</address>
       <note>will be chosen if _resources_ are not found at connections in the 'primary' outbound</note>
      </element>
     </connections>
    </element>
   </groups>
  </outbound>
  <reverse>
   <inbound>
    <groups>
     <element>
      <alias>unique-alias-name</alias>
      <note>connect to other reverse outbound that is listening on this port - then treat it as a regular inbound</note>
      <limit>
       <messages>42</messages>
      </limit>
      <connections>
       <element>
        <address>localhost:7780</address>
        <note>one of possible many addresses to connect to</note>
       </element>
      </connections>
     </element>
    </groups>
   </inbound>
   <outbound>
    <groups>
     <element>
      <alias>primary</alias>
      <note>listen for connection from reverse inbound - then treat it as a regular outbound</note>
      <connections>
       <element>
        <address>localhost:7780</address>
        <note>one of possible many listining addresses.</note>
       </element>
      </connections>
     </element>
     <element>
      <alias>secondary</alias>
      <note>onther instance (proces) that handles (multiplexed) traffic on it's own</note>
      <connections>
       <element>
        <address>localhost:7781</address>
        <note>one of possible many listining addresses.</note>
       </element>
      </connections>
     </element>
    </groups>
   </outbound>
  </reverse>
 </gateway>
 <queue>
  <note>retry.count - if number of rollbacks is greater, message is moved to error-queue  retry.delay - the amount of time before the message is available for consumption, after rollback
</note>
  <default>
   <queue>
    <retry>
     <count>3</count>
     <delay>20s</delay>
    </retry>
   </queue>
   <directory>${CASUAL_DOMAIN_HOME}/queue/groups</directory>
  </default>
  <groups>
   <element>
    <alias>A</alias>
    <note>will get default queuebase: ${CASUAL_DOMAIN_HOME}/queue/groupA.gb</note>
    <queues>
     <element>
      <name>a1</name>
     </element>
     <element>
      <name>a2</name>
      <retry>
       <count>10</count>
       <delay>100ms</delay>
      </retry>
      <note>after 10 rollbacked dequeues, message is moved to a2.error</note>
     </element>
     <element>
      <name>a3</name>
     </element>
     <element>
      <name>a4</name>
     </element>
    </queues>
   </element>
   <element>
    <alias>B</alias>
    <queuebase>/some/fast/disk/queue/groupB.qb</queuebase>
    <queues>
     <element>
      <name>b1</name>
     </element>
     <element>
      <name>b2</name>
      <retry>
       <count>20</count>
      </retry>
      <note>after 20 rollbacked dequeues, message is moved to b2.error. retry.delay is 'inherited' from default, if any</note>
     </element>
    </queues>
   </element>
   <element>
    <queuebase>:memory:</queuebase>
    <note>group is an in-memory queue, hence no persistence</note>
    <queues>
     <element>
      <name>c1</name>
     </element>
     <element>
      <name>c2</name>
     </element>
    </queues>
    <name>C</name>
   </element>
  </groups>
  <forward>
   <default>
    <service>
     <instances>3</instances>
     <reply>
      <delay>2s</delay>
     </reply>
    </service>
    <queue>
     <instances>1</instances>
     <target>
      <delay>500ms</delay>
     </target>
    </queue>
   </default>
   <groups>
    <element>
     <alias>forward-group-1</alias>
     <services>
      <element>
       <source>b1</source>
       <instances>4</instances>
       <target>
        <service>casual/example/echo</service>
       </target>
       <reply>
        <queue>a3</queue>
        <delay>10ms</delay>
       </reply>
      </element>
     </services>
     <queues>
      <element>
       <source>c1</source>
       <target>
        <queue>a4</queue>
       </target>
      </element>
     </queues>
    </element>
    <element>
     <alias>forward-group-2</alias>
     <services>
      <element>
       <source>b2</source>
       <target>
        <service>casual/example/echo</service>
       </target>
      </element>
     </services>
    </element>
   </groups>
  </forward>
 </queue>
</domain>

````
