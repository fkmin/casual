import casual.server.buffer as buffer
import casual.xatmi.xatmi as xatmi

def userlog( message: bytes, category = b'debug'):
    """
    userlog function hooking in on casual log c-api
    """

    if isinstance(message, str):
        message = message.encode('utf-8')
    if isinstance(category, str):
        category = category.encode('utf-8')

    xatmi.casual_user_log( category, message)