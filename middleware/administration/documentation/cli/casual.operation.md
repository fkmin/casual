# casual

```shell
host# casual --help 

DESCRIPTION
  
  casual administration CLI
  
  To get more detailed help, use any of:
  casual --help <option>
  casual <option> --help
  casual --help <option> <option> 
  
  Where <option> is one of the listed below

OPTIONS        c  value                             vc    description                                                                             
-------------  -  --------------------------------  ----  ----------------------------------------------------------------------------------------
[0;33m--information[0m  [0m?[0m  [0m<value>                         [0m  [0m   *[0m  [0mcollect general aggregated information about the domain                                 [0m
[0;33mdomain       [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mlocal casual domain related administration                                              [0m
[0;33mservice      [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mservice related administration                                                          [0m
[0;33mqueue        [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mqueue related administration                                                            [0m
[0;33mtransaction  [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mtransaction related administration                                                      [0m
[0;33mgateway      [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mgateway related administration                                                          [0m
[0;33mcall         [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mgeneric service call                                                                    [0m
[0;33mdescribe     [0m  [0m?[0m  [0m<service> [json, yaml, xml, ini][0m  [0m1..2[0m  [0mservice describer                                                                       [0m
[0;33mbuffer       [0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mbuffer related 'tools'                                                                  [0m
[0;33mconfiguration[0m  [0m?[0m  [0m                                [0m  [0m    [0m  [0mconfiguration utility - does NOT actively configure anything                            [0m
[0;33m--color      [0m  [0m?[0m  [0m[true, false, auto]             [0m  [0m   1[0m  [0mset/unset color (default:  )                                                            [0m
[0;33m--header     [0m  [0m?[0m  [0m[true, false]                   [0m  [0m   1[0m  [0mset/unset header (default: true)                                                        [0m
[0;33m--precision  [0m  [0m?[0m  [0m<value>                         [0m  [0m   1[0m  [0mset number of decimal points used for output (default: 3)                               [0m
[0;33m--block      [0m  [0m?[0m  [0m[true, false]                   [0m  [0m   1[0m  [0mset/unset blocking - if false return control to user as soon as possible (default: true)[0m
[0;33m--verbose    [0m  [0m?[0m  [0m[true, false]                   [0m  [0m   1[0m  [0mverbose output (default: false)                                                         [0m
[0;33m--porcelain  [0m  [0m?[0m  [0m[true, false]                   [0m  [0m   1[0m  [0measy to parse output format (default: false)                                            [0m
[0;33m--help       [0m  [0m?[0m  [0m<value>                         [0m  [0m   *[0m  [0muse --help <option> to see further details                                              [0m

```
