# casual gateway

```shell
host# casual --help gateway

  gateway [0..1]
        gateway related administration

    SUB OPTIONS
      -c, --list-connections [0..1]
            list all connections

      -l, --list-listeners [0..1]
            list all listeners

      --list-inbound-groups [0..1]
            list all inbound groups

      --list-outbound-groups [0..1]
            list all outbound groups

      --rediscover [0..1]
            rediscover all outbound connections

      --state [0..1]  (json, yaml, xml, ini) [0..1]
            gateway state

```
